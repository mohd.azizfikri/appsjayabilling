package com.example.jayabillingapartmen

import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

/*
* Created by Zee on 23/11/2020
* */

open class CoroutineContextProvider {
    open val main: CoroutineContext by lazy { Dispatchers.Main }
    open val io : CoroutineContext by lazy { Dispatchers.IO }
    open val default : CoroutineContext by lazy { Dispatchers.Default }
}