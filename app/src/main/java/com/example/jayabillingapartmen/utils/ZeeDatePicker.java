package com.example.jayabillingapartmen.utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.ContextThemeWrapper;

import com.example.jayabillingapartmen.R;

import java.util.Calendar;
import java.util.Date;

import static com.example.jayabillingapartmen.UtilsKt.stringToDate;

public class ZeeDatePicker extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Date getDate = stringToDate("12-12-1990","dd-MM-yyyy");
        String getLaut = null;
        try {
            getDate = stringToDate(getArguments().getString("date"), "dd MMM yyyy");
            getLaut = getArguments().getString("layout");
        }catch (Exception e){

        }

        final Calendar c = Calendar.getInstance();
        c.setTime(getDate);

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        if (getLaut != null) {
            return new DatePickerDialog(new ContextThemeWrapper(getActivity(), R.style.AlertDialogCustomEditProfil),
                    (DatePickerDialog.OnDateSetListener) getActivity(), year, month, day);
        }else {
            return new DatePickerDialog(new ContextThemeWrapper(getActivity(), R.style.AlertDialogCustom),
                    (DatePickerDialog.OnDateSetListener) getActivity(), year, month, day);
        }
    }
}
