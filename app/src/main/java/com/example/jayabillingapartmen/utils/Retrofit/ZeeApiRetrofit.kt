package com.example.jayabillingapartmen.Retrofit

import android.database.Observable
import com.example.jayabillingapartmen.content.history.model.HistoryBill
import com.example.jayabillingapartmen.content.notification.model.NotificationList
import com.example.jayabillingapartmen.content.setting.model.requestChange
import com.example.jayabillingapartmen.login.model.profile.Profile
import com.example.jayabillingapartmen.login.model.requestLogin
import com.example.jayabillingapartmen.login.model.responLogin
import com.example.jayabillingapartmen.utils.mainactivitymodel.TotalReport
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


interface ZeeApiRetrofit {
    //Auth
    @Headers("Content-Type: application/json")
    @POST("http://167.99.76.242:8080/" + "api/v1/auth/login")
    fun login(
            @Body model: requestLogin)
            : Call<responLogin>

    @Headers("accept: application/json")
    @GET("http://167.99.76.242:8080/" + "api/v1/auth/profile")
    fun get_profile(
            @Header("Authorization") Authorization  : String)
            : Call<Profile>

    //Home
    @Headers("accept: application/json")
    @GET("http://167.99.76.242:8080/" + "api/v1/home/total_report")
    fun get_total_report(
            @Header("Authorization") Authorization  : String)
            : Call<TotalReport>

    //Notification
    @Headers("accept: application/json")
    @GET("http://167.99.76.242:8080/" + "api/v1/notification/all")
    fun get_notification(
            @Header("Authorization") Authorization  : String)
            : Call<NotificationList>

    //Electrical Bill
    @Headers("accept: application/json")
    @GET("http://167.99.76.242:8080/" + "api/v1/electricity_bill/all")
    fun get_history_electric_bill(
            @Header("Authorization") Authorization  : String)
            : Call<HistoryBill>

    @Headers("accept: application/json")
    @GET("http://167.99.76.242:8080/" + "api/v1/electricity_bill/all")
    fun filter_history_electric_bill(
            @Header("Authorization") Authorization  : String,
            @Query("from") from : String,
            @Query("to") to : String)
            : Call<HistoryBill>

    @Multipart
    @POST("http://167.99.76.242:8080/" + "api/v1/electricity_bill/create")
    fun create_report_electric(
        @Header("Authorization") Authorization  : String,
        @Part("unit_id") unit_id: RequestBody?,
        @Part("bill_number") bill_number: RequestBody?,
        @Part image: MultipartBody.Part?,
        @Part("current_meter") current_meter: RequestBody?
    ): Call<ResponseBody?>

    //Water Bill
    @Headers("accept: application/json")
    @GET("http://167.99.76.242:8080/" + "api/v1/water_bill/all")
    fun get_history_water_bill(
            @Header("Authorization") Authorization  : String)
            : Call<HistoryBill>

    @Headers("accept: application/json")
    @GET("http://167.99.76.242:8080/" + "api/v1/water_bill/all")
    fun filter_history_water_bill(
            @Header("Authorization") Authorization  : String,
            @Query("from") from : String,
            @Query("to") to : String)
            : Call<HistoryBill>

    @Multipart
    @POST("http://167.99.76.242:8080/" + "api/v1/water_bill/create")
    fun create_report_water(
            @Header("Authorization") Authorization  : String,
            @Part("unit_id") unit_id: RequestBody?,
            @Part("bill_number") bill_number: RequestBody?,
            @Part image: MultipartBody.Part?,
            @Part("current_meter") current_meter: RequestBody?
    ): Call<ResponseBody?>

    //Setting
    @Headers("Content-Type: application/json")
    @POST("http://167.99.76.242:8080/" + "api/v1/profile/update_phone")
    fun changeNumber(
            @Header("Authorization") Authorization  : String,
            @Body model: requestChange)
            : Call<Any>
/*
    //Notification
    @Headers("Content-Type: application/json")
    @PUT("http://pesanku.biz:8080/notification/{type}/{notif_id}")
    fun reportDeliveryFB(@Body model: StatusDRModel,
                         @Path("type") type : String,
                         @Path("notif_id") notif_id : String) : Call<ReturnDRModel>

    //Submit Polling
    @Headers("Content-Type: application/json")
    @PUT("http://pesanku.biz:8080/notification/{type}/{notif_id}")
    fun submitPolling(@Body model: SubmitPollingModel,
                         @Path("type") type : String,
                         @Path("notif_id") notif_id : String) : Call<ReturnDRModel>

    //Auth
    @Headers("accept: application/json")
    @GET("http://pesanku.biz:4000/auth/check")
    fun checkAccount(@Query("phonenumber") phonenumber : String, @Query("machineId") machineId : String) : Call<CheckAccModel>

    @Headers("accept: application/json")
    @POST("http://pesanku.biz:4000/auth/code")
    fun registerGetCode(@Query("phonenumber") phonenumber : String, @Query("machineId") machineId : String)  : Call<CodeModel>

    @Headers("accept: application/json")
    @POST("http://pesanku.biz:4000/auth/register")
    fun registerAccount(@Query("registrationId") registrationId : String, @Query("code") code : String) : Call<RegistModel>

    //User
    @GET("http://pesanku.biz:4000/user")
    fun checkSession(@Header("Authorization") Authorization  : String) : Call<ModelCheckData>

    @Headers("Content-Type: application/json")
    @PUT("http://pesanku.biz:4000/user/data")
    fun updateBiodata(@Header("Authorization") Authorization  : String, @Body model: StatusDRModel) : Call<String>

    @Headers("Content-Type: application/json")
    @PUT("http://pesanku.biz:4000/user/firebase")
    fun updateFbaseToken(@Header("Authorization") Authorization  : String, @Query("firebaseId") firebaseId : String) : Call<FbaseTokenModel>
*/

    /*@GET("https://mpaz.byonchat.com/v1/index.php/api/courier/"+"transaction/histories")
     fun getListHistory(@Header("token") token  : String) : Call<ArrayList<ListOpen>>

     @GET("https://mpaz.byonchat.com/v1/index.php/api/courier/"+"transaction/{id}")
     fun getDetail(@Header("token") token  : String, @Path("id") id : String) : Call<DetailOrderModel>

     @POST("https://mpaz.byonchat.com/v1/index.php/api/courier/"+"transaction/{id}/delivery")
     fun submitDelivery(@Header("token") token  : String, @Path("id") id : String) : Call<DetailOrderModel>

     @FormUrlEncoded
     @POST("https://mpaz.byonchat.com/v1/index.php/api/courier/"+"login")
     fun login(@Field("username") username  : String, @Field("password") password  : String) : Call<LoginModel>

     @FormUrlEncoded
     @POST("https://mpaz.byonchat.com/v1/index.php/api/courier/"+"transaction/{id}/complete")
     fun completeOrder(@Header("token") token  : String, @Path("id") id : String, @Field("finger_code") finger_code  : String) : Call<DetailOrderModel>
 */
}