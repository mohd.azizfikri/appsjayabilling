package com.example.jayabillingapartmen


import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import javax.net.ssl.HttpsURLConnection

/**
 * Created by Zee on 25/11/2020
 */

class ApiRepository {

    fun getRequest(url: String): Deferred<String> = GlobalScope.async {
        URL(url).readText()
    }

    fun postRequest(url : String,postDataParams: HashMap<String, String>?,headers: HashMap<String, String>?) : Deferred<String> = GlobalScope.async {
        sendPostRequest(url,postDataParams,headers)
    }

    private fun sendPostRequest(requestURL: String, postDataParams: HashMap<String, String>? , headers : HashMap<String,String>?): String {

        val url: URL
        var response = ""
        try {
            url = URL(requestURL)

            val conn = url.openConnection() as HttpURLConnection
            if (headers != null){
                for ((key, value) in headers){
                    conn.setRequestProperty(key,value)
                }
            }
            conn.requestMethod = "POST"
            conn.doInput = true
            conn.doOutput = true

            val os = conn.outputStream
            val writer = BufferedWriter(
                OutputStreamWriter(os, "UTF-8")
            )
            if (postDataParams != null){
                writer.write(getPostDataString(postDataParams))
            }

            writer.flush()
            writer.close()
            os.close()
            response = "${conn.responseCode}"

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return response
    }

    @Throws(UnsupportedEncodingException::class)
    private fun getPostDataString(params: HashMap<String, String>): String {
        val result = StringBuilder()
        var first = true
        for ((key, value) in params) {
            if (first)
                first = false
            else
                result.append("&")

            result.append(URLEncoder.encode(key, "UTF-8"))
            result.append("=")
            result.append(URLEncoder.encode(value, "UTF-8"))
        }

        return result.toString()
    }
}