package com.example.jayabillingapartmen.utils.mainactivitymodel

data class Data(
    val electricity: Electricity,
    val water: Water
)