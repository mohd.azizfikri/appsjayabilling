package com.example.jayabillingapartmen.utils.mainactivitymodel

data class TotalReport(
    val `data`: Data,
    val message: String,
    val status: Boolean
)