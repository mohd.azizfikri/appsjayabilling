package com.example.jayabillingapartmen

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Point
import android.support.design.widget.CoordinatorLayout
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.DatePicker
import android.widget.FrameLayout
import android.widget.ScrollView
import android.widget.Toast
import com.example.jayabillingapartmen.database.ProfileDB
import com.example.jayabillingapartmen.login.model.profile.Profile
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


/*
* Created by Zee on 24/11/2020
* */

val BASE_URL = "http://167.99.76.242:8080/"
val no_bearer = "Bearer "
val server_error = "Signin failure"

fun View?.findSuitableParent() : ViewGroup? {
    /*
    * Created by fabio_negri on GitHub
    * */
    var view = this
    var fallback: ViewGroup? = null
    do {
        if (view is CoordinatorLayout) {
            // We've found a CoordinatorLayout, use it
            return view
        } else if (view is FrameLayout) {
            if (view.id == android.R.id.content) {
                // If we've hit the decor content view, then we didn't find a CoL in the
                // hierarchy, so use it.
                return view
            } else {
                // It's not the content view but we'll use it as our fallback
                fallback = view
            }
        }

        if (view != null) {
            // Else, we will loop and crawl up the view hierarchy and try to find a parent
            val parent = view.parent
            view = if (parent is View) parent else null
        }
    } while (view != null)

    // If we reach here then we didn't find a CoL or a suitable content view so we'll fallback
    return fallback
}

fun log(content : String?){
    Log.w("zeeLogTest", content.toString())
}

fun Context.toast(content : String?){
    try {
        Toast.makeText(this,content,Toast.LENGTH_SHORT).show()
    }catch (e: Exception){
        e.printStackTrace()
        log("https error back: " + e.localizedMessage)
    }
}

fun Context.toast(content : String?, type: Int){
    try {

        Toast.makeText(this,content, type).show()
    }catch (e: Exception){
        e.printStackTrace()
        log("https error back: " + e.localizedMessage)
    }
}

fun View.visible(){
    visibility = View.VISIBLE
}

fun View.invisible(){
    visibility = View.INVISIBLE
}

fun View.gone(){
    visibility = View.GONE
}

fun Activity.actionExitApps(){
    val builder1 = AlertDialog.Builder(this)
    builder1.setMessage("Apakah anda akan keluar dari Aplikasi")
    builder1.setCancelable(true)

    builder1.setPositiveButton(
        "Yes", { dialogInterface: DialogInterface, i: Int ->
                this.finish()
        })

    builder1.setNegativeButton(
        "No", { dialogInterface: DialogInterface, i: Int ->
            dialogInterface.cancel()
        })

    val alert11 = builder1.create()
    alert11.show()
}

fun getSelfData(ct: Context): Profile{
    val bioDT: Profile
    val bioDB = ProfileDB(ct)
    bioDB.open()
    bioDT = bioDB.retrieveRoomsbyID("1")
    bioDB.close()

    return bioDT
}

fun getAccessToken(ct: Context): String{
    val token: String
    val bioDB = ProfileDB(ct)
    bioDB.open()
    token = no_bearer + bioDB.retrieveAccessToken("1")
    bioDB.close()

    return token
}

fun deleteAllData(ct: Context){
    val bioDB = ProfileDB(ct)
    bioDB.open()
    bioDB.deleteAllData()
    bioDB.close()
}

fun removeHtmlTag(text: String): String {
    var str = text
    str = str.replace("\\<.*?\\>".toRegex(), "")
    str = str.replace("&nbsp;","")

    return str
}

fun scrollToView(scrollViewParent: ScrollView, view: View) {
    // Get deepChild Offset
    val childOffset = Point()
    getDeepChildOffset(scrollViewParent, view.parent, view, childOffset)
    // Scroll to child.
    scrollViewParent.smoothScrollTo(0, childOffset.y)
}

fun getDeepChildOffset(mainParent: ViewGroup, parent: ViewParent, child: View, accumulatedOffset: Point) {
    val parentGroup = parent as ViewGroup
    accumulatedOffset.x += child.left
    accumulatedOffset.y += child.top
    if (parentGroup == mainParent) {
        return
    }
    getDeepChildOffset(mainParent, parentGroup.parent, parentGroup, accumulatedOffset)
}

fun getCurrentDate(): String {
    val calendar = Calendar.getInstance()
    val mdformat = SimpleDateFormat("dd-MM-yyyy")

    return mdformat.format(calendar.time)
}
fun getCurrentDateAsDate(): Date {
    val date = stringToDate(getCurrentDate(),"dd-MM-yyyy")

    return date
}

fun getDateFromDatePicker(datePicker: DatePicker): String {
    val day = datePicker.dayOfMonth
    val month = datePicker.month
    val year = datePicker.year

    val calendar = Calendar.getInstance()
    calendar.set(year, month, day)

    val format = SimpleDateFormat("dd-MM-yyyy")

    return format.format(calendar.time)
}

fun setDateFormat(date: String, formatAwal: String): String {
    val format = SimpleDateFormat(formatAwal)
    val finals = SimpleDateFormat("dd MMM yyyy")
    val spDate = format.parse(date)

    return finals.format(spDate)
}

fun changeDateFormat(date: String, formatAwal: String, formatAkhir: String): String {
    val format = SimpleDateFormat(formatAwal)
    val finals = SimpleDateFormat(formatAkhir)
    val spDate = format.parse(date)

    return finals.format(spDate)
}

fun stringToDate(date: String, format: String): Date {
    val formater = SimpleDateFormat(format)
    val spDate = formater.parse(date)

    return spDate
}

fun yesterday(): Date? {
    val cal = Calendar.getInstance()
    cal.add(Calendar.DATE, -1)
    return cal.time
}

fun getYesterdayDate(): Date? {
    val dateFormat: DateFormat = SimpleDateFormat("yyyy/MM/dd")
    val date = stringToDate(dateFormat.format(yesterday()), "yyyy/MM/dd")
    return date
}