package com.example.jayabillingapartmen.utils.`interface`

import com.example.jayabillingapartmen.content.history.model.HistoryBill
import com.example.jayabillingapartmen.content.notification.model.Data
import com.example.jayabillingapartmen.content.notification.model.NotificationList
import com.example.jayabillingapartmen.login.model.profile.Profile
import com.example.jayabillingapartmen.login.model.responLogin
import com.example.jayabillingapartmen.utils.mainactivitymodel.TotalReport

/*
 * Created by Zee on 24/11/2020
 * Ini tempat berkumpulnya view interface untuk Presenter
 * */

interface BaseView {
    fun showLoading()
    fun hideLoading()
}

interface onClickItem{
    fun OnClick(item : Any)
}

interface errorView{
    fun errorResponse(cause: String)
}

interface SignInView : errorView{
    fun onLoginSuccess(item: responLogin)
    fun onGetProfile(token: String, item: Profile)
}

interface NotificationListView : errorView{
    fun onGetNotification(item: NotificationList)
}

interface MainActivityView : errorView{
    fun onGetTotalReport(item: TotalReport)
}

interface ChangeNumberView : errorView{
    fun onSuccessChanging(item: Any)
}

interface CreateReportView : errorView{
    fun onSuccessPosting(item: Any)
}

interface HistoryBillView : errorView{
    fun onGetTotalBill(item: HistoryBill)
}
