package com.example.jayabillingapartmen

import com.example.jayabillingapartmen.BuildConfig

/**
 * Created by Zee on 25/11/2020
 * Class object untuk menyimpan URL Api
 */

object Api {
    fun registToken(phone : String , token : String) : String {
        // URL untuk meregister token ke API yang dibuat Ganang
        return "${"https://bbn.byonchat.com/receiveMO/insertToken.php"}?phone=$phone&token=$token"
    }
}