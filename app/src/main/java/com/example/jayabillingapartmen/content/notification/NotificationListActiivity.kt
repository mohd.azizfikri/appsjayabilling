package com.example.jayabillingapartmen.content.notification

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import com.example.jayabillingapartmen.R
import com.example.jayabillingapartmen.content.addition.SpanNoticationActivity
import com.example.jayabillingapartmen.content.notification.adapter.BannerAdapter
import com.example.jayabillingapartmen.content.notification.adapter.HistoryBillAdapter
import com.example.jayabillingapartmen.content.notification.adapter.NotificationListAdapter
import com.example.jayabillingapartmen.content.notification.model.Data
import com.example.jayabillingapartmen.content.notification.model.NotificationList
import com.example.jayabillingapartmen.getAccessToken
import com.example.jayabillingapartmen.gone
import com.example.jayabillingapartmen.toast
import com.example.jayabillingapartmen.utils.`interface`.NotificationListView
import com.example.jayabillingapartmen.utils.`interface`.onClickItem
import kotlinx.android.synthetic.main.activity_notification_list_actiivity.*
import kotlinx.android.synthetic.main.activity_span_notication.*
import java.util.*

class NotificationListActiivity : AppCompatActivity(), NotificationListView {
    private lateinit var presenter: NotificationListPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification_list_actiivity)
        supportActionBar?.hide()

        presenter = NotificationListPresenter(this)
        presenter.getNotification(getAccessToken(applicationContext))
        banner_slide()
    }

    fun banner_slide(){

        var currentPage = 0
        val timer: Timer
        val DELAY_MS: Long = 1000    // delay in milliseconds before task is to be executed
        val PERIOD_MS: Long = 7000  // time in milliseconds between successive task executions.

        vp_banner.adapter = BannerAdapter(this)

        /*After setting the adapter use the timer */
        val handler = Handler()
        val Update = Runnable() {
            run {
                if (currentPage == 1) {
                    currentPage = 0
                }else{
                    currentPage++
                }
                vp_banner?.setCurrentItem(currentPage, true)
            }
        }

        timer = Timer() // This will create a new Thread
        timer.schedule(object : TimerTask() { // task to be scheduled
            override fun run() {
                handler.post(Update)
            }
        }, DELAY_MS, PERIOD_MS)
    }

    override fun onGetNotification(item: NotificationList) {
        //TODO tambahkan isi arraylist untuk adapter (NotificationListAdapter)
        recycle_history.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = NotificationListAdapter(context, item.data, object : onClickItem{
                override fun OnClick(item: Any) {
                    val data = item as Data

                    val intent = Intent(context, SpanNoticationActivity::class.java)
                    intent.putExtra("title", data.date)
                    intent.putExtra("content", data.content)
                    startActivity(intent)
                }
            })
        }

        notif_load.gone()
    }

    override fun errorResponse(cause: String) {
        toast(cause)
    }
}