package com.example.jayabillingapartmen.content.history

import com.example.jayabillingapartmen.CoroutineContextProvider
import com.example.jayabillingapartmen.Retrofit.ZeeRetrofit
import com.example.jayabillingapartmen.changeDateFormat
import com.example.jayabillingapartmen.content.createreport.model.ErrorResponse
import com.example.jayabillingapartmen.content.history.model.HistoryBill
import com.example.jayabillingapartmen.log
import com.example.jayabillingapartmen.server_error
import com.example.jayabillingapartmen.utils.`interface`.HistoryBillView
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HistoryActivityPresenter (private val view: HistoryBillView) {
    private val ctx: CoroutineContextProvider =
            CoroutineContextProvider()

    fun getTotalReport(menuID: Int, token: String){
        GlobalScope.launch(ctx.default) {
            if(menuID == 3) {
                ZeeRetrofit().services.get_history_electric_bill(token).enqueue(object :
                        Callback<HistoryBill> {
                    override fun onFailure(call: Call<HistoryBill>, t: Throwable) {
                        log(t.localizedMessage)
                    }

                    override fun onResponse(call: Call<HistoryBill>, response: Response<HistoryBill>) {
                        if(response.code() == 200){
                            response.body()?.let { view.onGetTotalBill(it) }
                        }else{
                            try {
                                val res: ErrorResponse = Gson().fromJson(response.errorBody()?.charStream(), ErrorResponse::class.java)
                                view.errorResponse(res.message)
                            }catch (e: Exception){
                                view.errorResponse(server_error)
                            }
                        }
                    }

                })
            }else{
                ZeeRetrofit().services.get_history_water_bill(token).enqueue(object :
                        Callback<HistoryBill> {
                    override fun onFailure(call: Call<HistoryBill>, t: Throwable) {
                        log(t.localizedMessage)
                    }

                    override fun onResponse(call: Call<HistoryBill>, response: Response<HistoryBill>) {
                        if(response.code() == 200){
                            response.body()?.let { view.onGetTotalBill(it) }
                        }else{
                            try {
                                val res: ErrorResponse = Gson().fromJson(response.errorBody()?.charStream(), ErrorResponse::class.java)
                                view.errorResponse(res.message)
                            }catch (e: Exception){
                                view.errorResponse(server_error)
                            }
                        }

                    }

                })
            }
        }
    }

    fun getTotalReport(menuID: Int, token: String, start_from: String, end_to: String){
        val date_start = changeDateFormat(start_from, "dd-MM-yyyy", "yyyy-MM-dd")
        val date_end = changeDateFormat(end_to, "dd-MM-yyyy", "yyyy-MM-dd")

        GlobalScope.launch(ctx.default) {
            if(menuID == 3) {
                ZeeRetrofit().services.filter_history_electric_bill(token, date_start, date_end).enqueue(object :
                        Callback<HistoryBill> {
                    override fun onFailure(call: Call<HistoryBill>, t: Throwable) {
                        log(t.localizedMessage)
                    }

                    override fun onResponse(call: Call<HistoryBill>, response: Response<HistoryBill>) {
                        if(response.code() == 200){
                            response.body()?.let { view.onGetTotalBill(it) }
                        }else{
                            try {
                                val res: ErrorResponse = Gson().fromJson(response.errorBody()?.charStream(), ErrorResponse::class.java)
                                view.errorResponse(res.message)
                            }catch (e: Exception){
                                view.errorResponse(server_error)
                            }
                        }
                    }

                })
            }else{
                ZeeRetrofit().services.filter_history_water_bill(token, date_start, date_end).enqueue(object :
                        Callback<HistoryBill> {
                    override fun onFailure(call: Call<HistoryBill>, t: Throwable) {
                        log(t.localizedMessage)
                    }

                    override fun onResponse(call: Call<HistoryBill>, response: Response<HistoryBill>) {
                        if(response.code() == 200){
                            response.body()?.let { view.onGetTotalBill(it) }
                        }else{
                            try {
                                val res: ErrorResponse = Gson().fromJson(response.errorBody()?.charStream(), ErrorResponse::class.java)
                                view.errorResponse(res.message)
                            }catch (e: Exception){
                                view.errorResponse(server_error)
                            }
                        }

                    }

                })
            }
        }
    }
}