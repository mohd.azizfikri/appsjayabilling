package com.example.jayabillingapartmen.content.notification.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import com.example.jayabillingapartmen.content.notification.adapter.viewholder.NotificationListViewHolder
import com.example.jayabillingapartmen.content.notification.model.Data
import com.example.jayabillingapartmen.utils.`interface`.onClickItem
import java.util.*
import kotlin.collections.ArrayList

class NotificationListAdapter (private val context: Context, private val items : ArrayList<Data>,
                               private val listener: onClickItem) : RecyclerView.Adapter<NotificationListViewHolder>() {
    private var itemList: ArrayList<Data> = items
    private var itemListFiltered: ArrayList<Data>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return NotificationListViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: NotificationListViewHolder, position: Int) {
        val item : Data = itemList[position]

        holder.bind(item)

        holder.itemView.setOnClickListener {
            listener.OnClick(item)
        }
    }

    fun getFilter(): Filter? {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    itemListFiltered = items
                } else {
                    val filteredList: MutableList<Data> = ArrayList()
                    if (itemList != null) {
                        for (row in itemList) {

                            // name match condition. this might differ depending on your requirement
                            // here we are looking for name or phone number match
                            if (row.content.toLowerCase(Locale.ROOT).contains(charString.toLowerCase(Locale.ROOT)) || row.date.contains(charSequence.toString())) {
                                filteredList.add(row)
                            }
                        }
                    }
                    itemListFiltered = filteredList as ArrayList<Data>
                }
                val filterResults = FilterResults()
                filterResults.values = itemListFiltered
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                itemListFiltered = results?.values as ArrayList<Data>
                itemList = itemListFiltered!!

                // refresh the list with filtered data
                notifyDataSetChanged()
            }
        }
    }

    override fun getItemCount(): Int = itemList.size

}