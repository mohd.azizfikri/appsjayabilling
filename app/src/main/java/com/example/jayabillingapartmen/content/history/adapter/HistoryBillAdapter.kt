package com.example.jayabillingapartmen.content.notification.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import com.example.jayabillingapartmen.content.history.model.BillData
import com.example.jayabillingapartmen.content.notification.adapter.viewholder.HistoryBillViewHolder
import com.example.jayabillingapartmen.utils.`interface`.onClickItem

class HistoryBillAdapter (private val context: Context, private val items : ArrayList<BillData>,
                          private val listener: onClickItem) : RecyclerView.Adapter<HistoryBillViewHolder>() {
    /*  */
    private var contactList: ArrayList<BillData> = items
    private var contactListFiltered: ArrayList<BillData>? = null
/*  */

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryBillViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return HistoryBillViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: HistoryBillViewHolder, position: Int) {
        val item : BillData = contactList[position]

        holder.bind(item)

        holder.itemView.setOnClickListener {
            listener.OnClick(item)
        }
    }
/*   */

    fun getFilter(): Filter? {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    contactListFiltered = items
                } else {
                    val filteredList: MutableList<BillData> = ArrayList()
                    if (contactList != null) {
                        for (row in contactList) {

                            // name match condition. this might differ depending on your requirement
                            // here we are looking for name or phone number match
                            if (row.name.toLowerCase().contains(charString.toLowerCase()) || row.bill_number.contains(charSequence.toString())) {
                                filteredList.add(row)
                            }
                        }
                    }
                    contactListFiltered = filteredList as ArrayList<BillData>
                }
                val filterResults = FilterResults()
                filterResults.values = contactListFiltered
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                contactListFiltered = results?.values as ArrayList<BillData>
                contactList = contactListFiltered!!

                // refresh the list with filtered data
                notifyDataSetChanged()
            }
        }
    }
/*  */

    override fun getItemCount(): Int = contactList.size

}