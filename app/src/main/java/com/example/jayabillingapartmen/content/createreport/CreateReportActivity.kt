package com.example.jayabillingapartmen.content.createreport

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.view.MenuItem
import android.widget.Toast
import com.example.jayabillingapartmen.*
import com.example.jayabillingapartmen.content.addition.ReportStatusActivity
import com.example.jayabillingapartmen.utils.`interface`.CreateReportView
import com.google.zxing.integration.android.IntentIntegrator
import com.google.zxing.integration.android.IntentResult
import com.journeyapps.barcodescanner.CaptureActivity
import kotlinx.android.synthetic.main.activity_create_report.*
import java.io.*
import java.util.*

class CreateReportActivity : AppCompatActivity(), CreateReportView {
    private lateinit var presenter: CreateReportPresenter
    private lateinit var filePicture: File
    private var menuID: Int = 1
    private var pageId = 1

    //Permission Array
    private val ALL_PERMISSIONS_RESULT = 107
    private var permissionsToRequest: ArrayList<String>? = null
    private var permissionsRejected: ArrayList<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_report)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        presenter = CreateReportPresenter(this)

        validationMenu()
        cekPermision()

        btn_next.setOnClickListener {
            if (ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED){
                cekPermision()
                toast(getString(R.string.permission_hardrequest_title), Toast.LENGTH_LONG)
            }else if(ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED){
                cekPermision()
                toast(getString(R.string.permission_hardrequest_title), Toast.LENGTH_LONG)
            }else if(input_code.text.isNullOrEmpty()) {
                toast(getString(R.string.code_unit_empty), Toast.LENGTH_LONG)
            }else {
                page1.gone()
                page2.visible()
                pageId = 2
            }
        }

        btn_save.setOnClickListener {
            presenter.getTotalReport(
                menuID,
                getAccessToken(applicationContext),
                input_code.text.toString(),
                input_bill.text.toString(),
                input_meteran.text.toString(),
                filePicture
            )
        }

        gallery_pick.setOnClickListener {
            val scanIntegrator = IntentIntegrator(this)
            scanIntegrator.setPrompt("Scan")
            scanIntegrator.setBeepEnabled(true)
            //The following line if you want QR code
            scanIntegrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES)
            scanIntegrator.captureActivity = CaptureActivity::class.java
            scanIntegrator.setOrientationLocked(true)
            scanIntegrator.setBarcodeImageEnabled(true)
            scanIntegrator.initiateScan()
        }

        camera_pick.setOnClickListener {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, 22222)
        }
    }

    fun validationMenu(){
        menuID = intent.getIntExtra("menu",1)

        if(menuID == 1){
            supportActionBar?.title = getString(R.string.menu1)
        }else{
            supportActionBar?.title = getString(R.string.menu2)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 11111)
                onSelectFromGalleryResult(data)
            else if (requestCode == 22222)
                onCaptureImageResult(data)
            else if (requestCode == 49374)
                onScannedBarcode(scanningResult)
        }
    }

    private fun onSelectFromGalleryResult(data: Intent?) {
        var bm: Bitmap? = null
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(applicationContext.contentResolver, data.data)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        gallery_pick.setImageBitmap(bm)
    }

    private fun onCaptureImageResult(data: Intent?) {
        camera_pick.setPadding(10,10,10,10)

        val thumbnail = data?.extras!!["data"] as Bitmap?
        val bytes = ByteArrayOutputStream()
        thumbnail!!.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val destination =
            File(Environment.getExternalStorageDirectory(), System.currentTimeMillis().toString() + ".jpg")
        val fo: FileOutputStream
        try {
            destination.mkdirs()
            if(!destination.exists()) {
                destination.createNewFile()
            }else{
                destination.delete()
                destination.createNewFile()
            }
            fo = FileOutputStream(destination)
            fo.write(bytes.toByteArray())
            fo.close()
        } catch (e: FileNotFoundException) {
            toast("error1: " + e.localizedMessage)
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
            toast("error2: " + e.localizedMessage)
        }

        filePicture = destination
        camera_pick.setImageBitmap(thumbnail)
    }

    private fun onScannedBarcode(scanningResult: IntentResult){
        if (scanningResult != null) {
            if (scanningResult.contents != null) {
                val scanContent = scanningResult.contents.toString()
                input_code.setText(scanContent)
            }
        } else {
            Toast.makeText(this, "No scanning result, Please use the clear barcode!", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                //You can do whatever you want here
                if(pageId == 2){
                    page1.visible()
                    page2.gone()
                    pageId = 1
                }else {
                    finish()
                }
                return true
            }
        }
        return false
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if(pageId == 2){
                page1.visible()
                page2.gone()
                pageId = 1
            }else {
                finish()
            }
            return true
        }
        return false
    }

    override fun onSuccessPosting(item: Any) {
        val intent = Intent(this, ReportStatusActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun errorResponse(cause: String) {
        toast(cause)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {

        when (requestCode) {
            ALL_PERMISSIONS_RESULT -> {
                var someAccepted = false
                var someRejected = false
                for (perms in permissionsToRequest!!) {
                    if (hasPermission(perms)) {
                        someAccepted = true
                    } else {
                        someRejected = true
                        permissionsRejected!!.add(perms)
                    }
                }

                if (permissionsRejected!!.size > 0) {
                    someRejected = true
                }

                if (someAccepted) {
                    //  permissionSuccess.setVisibility(View.VISIBLE);
                    someRejected = false
                }
                if (someRejected) {
                    //   makePostRequestSnack();
                    permissionsRejected!!.clear()
                    someRejected = false

                    Toast.makeText(
                            this,
                            getString(R.string.permission_request_title),
                            Toast.LENGTH_SHORT
                    ).show()

                    val handler = Handler()
                    handler.postDelayed({ cekPermision() }, (2 * 1000).toLong())
                }
            }
        }
    }
    private fun cekPermision() {
        val permissions = ArrayList<String>()
        var resultCode = 0
        permissions.add(Manifest.permission.CAMERA)
        permissions.add(Manifest.permission.INTERNET)
        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)

        resultCode = ALL_PERMISSIONS_RESULT
        permissionsToRequest = findUnAskedPermissions(permissions)
        permissionsRejected = findRejectedPermissions(permissions)

        if (permissionsToRequest!!.size > 0) {//we need to ask for permissions
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsToRequest!!.toTypedArray(), resultCode)
            }
        }
    }

    private fun hasPermission(permission: String): Boolean {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return checkSelfPermission(permission) === PackageManager.PERMISSION_GRANTED
            }

        }
        return true
    }

    private fun findUnAskedPermissions(wanted: ArrayList<String>): ArrayList<String> {
        val result = ArrayList<String>()
        for (perm in wanted) {
            result.add(perm)
        }

        return result
    }

    private fun findRejectedPermissions(wanted: ArrayList<String>): ArrayList<String> {
        val result = ArrayList<String>()
        for (perm in wanted) {
            result.add(perm)
        }
        return result
    }

    private fun canMakeSmores(): Boolean {
        return Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1
    }
}