package com.example.jayabillingapartmen.content.addition

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v4.view.ViewPager
import android.text.Editable
import android.text.TextWatcher
import com.example.jayabillingapartmen.R
import com.example.jayabillingapartmen.ZeeCircleTransform
import com.example.jayabillingapartmen.content.notification.adapter.BannerAdapter
import com.example.jayabillingapartmen.getSelfData
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_span_notication.*
import java.util.*

class SpanNoticationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_span_notication)
        supportActionBar?.hide()

        banner_slide()
        implementDataProfile()
        search_func()

        notif_title.text = intent.getStringExtra("title")
        notif_subtitle.text = intent.getStringExtra("content")
    }

    fun banner_slide(){

        var currentPage = 0
        val timer: Timer
        val DELAY_MS: Long = 1000    // delay in milliseconds before task is to be executed
        val PERIOD_MS: Long = 7000  // time in milliseconds between successive task executions.

        val span_banner = findViewById<ViewPager>(R.id.span_banner)
        span_banner.adapter = BannerAdapter(this)

        /*After setting the adapter use the timer */
        val handler = Handler()
        val Update = Runnable() {
            run {
                if (currentPage == 1) {
                    currentPage = 0
                }else{
                    currentPage++
                }
                span_banner?.setCurrentItem(currentPage, true)
            }
        }

        timer = Timer() // This will create a new Thread
        timer.schedule(object : TimerTask() { // task to be scheduled
            override fun run() {
                handler.post(Update)
            }
        }, DELAY_MS, PERIOD_MS)
    }

    @SuppressLint("SetTextI18n")
    fun implementDataProfile(){
        val bioDT = getSelfData(applicationContext)
        nameProfile.text = "Halo, "+ bioDT.data.username + "!"
        if(bioDT.data.avatar.toString().startsWith("http")) {
            Picasso.with(applicationContext)
                .load(bioDT.data.avatar.toString())
                .transform(ZeeCircleTransform(100, 0))
                .fit()
                .into(imgProfile)
        }
    }

    fun search_func(){
        historySearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                /*  val text_x = intent.getStringExtra("title")
                  val text_y = intent.getStringExtra("content")
                  if (s.isNullOrEmpty()) {
                      notif_title.text = text_x
                      notif_subtitle.text = text_y
                  } else {
                      val spannable: Spannable = SpannableString(text_x)
                      (s).length.let {
                          spannable.setSpan(
                              ForegroundColorSpan(Color.RED),
                              s.length,
                              10,
                              Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                          )
                      }
                      notif_title.setText(spannable, TextView.BufferType.SPANNABLE)

                      val spannable2: Spannable = SpannableString(text_x)
                      (s).length.let {
                          spannable2.setSpan(
                              ForegroundColorSpan(Color.RED),
                              s.length,
                              10,
                              Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                          )
                      }
                      notif_subtitle.setText(spannable2, TextView.BufferType.SPANNABLE)
                  }*/
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
    }
}