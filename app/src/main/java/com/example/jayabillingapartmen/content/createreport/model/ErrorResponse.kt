package com.example.jayabillingapartmen.content.createreport.model

data class ErrorResponse(
    val `data`: Any,
    val message: String,
    val status: Boolean
)