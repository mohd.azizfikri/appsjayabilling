package com.example.jayabillingapartmen.content.history.model

data class HistoryBill(
    val `data`: ArrayList<BillData>,
    val message: String,
    val status: Boolean
)