package com.example.jayabillingapartmen.content.setting

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.example.jayabillingapartmen.R
import com.example.jayabillingapartmen.gone
import com.example.jayabillingapartmen.login.SignIn
import com.example.jayabillingapartmen.visible
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        buttonChNomor.setOnClickListener {
            val intent = Intent(this, ChangeNumberActivity::class.java)
            startActivity(intent)
        }

        buttonOut.setOnClickListener {
            showDialog(true)
        }

        submit_do.setOnClickListener {
            val intent = Intent(this, SignIn::class.java)
            startActivity(intent)
            finish()
        }

        cancel_do.setOnClickListener {
            showDialog(false)
        }
    }

    fun showDialog(show: Boolean){
        if (show){
            dialog_logout.visible()
            buttonChNomor.isClickable = false
            buttonOut.isClickable = false
        }else{
            dialog_logout.gone()
            buttonChNomor.isClickable = true
            buttonOut.isClickable = true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                //You can do whatever you want here
                finish()
                return true
            }
        }
        return false
    }
}