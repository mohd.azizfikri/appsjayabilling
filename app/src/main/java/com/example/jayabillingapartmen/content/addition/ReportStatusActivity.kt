package com.example.jayabillingapartmen.content.addition

import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import com.example.jayabillingapartmen.R
import com.example.jayabillingapartmen.gone
import kotlinx.android.synthetic.main.activity_report_status.*

class ReportStatusActivity : AppCompatActivity() {
    var trans = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_status)
        supportActionBar?.hide()

        trans = intent.getIntExtra("trans",0)

        if(trans == 1){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                image_status.setImageDrawable(getDrawable(R.drawable.ic_finish))
            }
            tv_report_status.text = getString(R.string.sukses_ganti_nmr)
            btn_mail.gone()
            btn_msg.gone()
            btn_wa.gone()
            tx_mail.gone()
            tx_msg.gone()
            tx_wa.gone()
        }

        submit.setOnClickListener {
            finish()
        }
    }
}