package com.example.jayabillingapartmen.content.setting

import com.example.jayabillingapartmen.CoroutineContextProvider
import com.example.jayabillingapartmen.Retrofit.ZeeRetrofit
import com.example.jayabillingapartmen.content.createreport.model.ErrorResponse
import com.example.jayabillingapartmen.content.setting.model.requestChange
import com.example.jayabillingapartmen.log
import com.example.jayabillingapartmen.server_error
import com.example.jayabillingapartmen.utils.`interface`.ChangeNumberView
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ChangeNumberPresenter(private val view: ChangeNumberView) {
    private val ctx: CoroutineContextProvider =
            CoroutineContextProvider()

    fun change_number(token: String, phone: String){
        GlobalScope.launch(ctx.default) {
            ZeeRetrofit().services.changeNumber(token, requestChange(phone)).enqueue(object :
                    Callback<Any> {
                override fun onFailure(call: Call<Any>, t: Throwable) {
                    log(t.localizedMessage)
                }

                override fun onResponse(call: Call<Any>, response: Response<Any>) {
                    if(response.code() == 200){
                        response.body()?.let {
                            view.onSuccessChanging(it)
                        }
                    }else{
                        try {
                            val res: ErrorResponse = Gson().fromJson(response.errorBody()?.charStream(), ErrorResponse::class.java)
                            view.errorResponse(res.message)
                        }catch (e: Exception){
                            view.errorResponse(server_error)
                        }
                    }
                }

            })
        }
    }
}