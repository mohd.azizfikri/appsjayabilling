package com.example.jayabillingapartmen.content.createreport

import com.example.jayabillingapartmen.CoroutineContextProvider
import com.example.jayabillingapartmen.Retrofit.ZeeRetrofit
import com.example.jayabillingapartmen.log
import com.example.jayabillingapartmen.utils.`interface`.CreateReportView
import com.example.jayabillingapartmen.utils.mainactivitymodel.TotalReport
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class CreateReportPresenter (private val view: CreateReportView) {
    private val ctx: CoroutineContextProvider =
        CoroutineContextProvider()

    fun getTotalReport(menuID: Int, token: String, s_unit_id: String, s_bill_number: String, s_current_meter: String, file: File){
        GlobalScope.launch(ctx.default) {

            //pass it like this
            val requestFile: RequestBody =
                RequestBody.create(MediaType.parse("multipart/form-data"), file)

            // MultipartBody.Part is used to send also the actual file name
            val image =
                MultipartBody.Part.createFormData("image", file.getName(), requestFile)

            // add another part within the multipart request
            val bill_number =
                RequestBody.create(MediaType.parse("multipart/form-data"), s_bill_number)

            val unit_id =
                RequestBody.create(MediaType.parse("multipart/form-data"), s_unit_id)

            val current_meter =
                RequestBody.create(MediaType.parse("multipart/form-data"), s_current_meter)

            if(menuID == 1) {
                ZeeRetrofit().services.create_report_electric(token, unit_id, bill_number, image, current_meter).enqueue(object :
                        Callback<ResponseBody?> {
                    override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                        log(t.localizedMessage)
                    }

                    override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                        response.body()?.let { view.onSuccessPosting(it) }
                    }

                })
            }else{
                ZeeRetrofit().services.create_report_water(token, unit_id, bill_number, image, current_meter).enqueue(object :
                        Callback<ResponseBody?> {
                    override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                        log(t.localizedMessage)
                    }

                    override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                        response.body()?.let { view.onSuccessPosting(it) }
                    }

                })
            }
        }
    }
}