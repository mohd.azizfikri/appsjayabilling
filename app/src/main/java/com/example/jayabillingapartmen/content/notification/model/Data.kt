package com.example.jayabillingapartmen.content.notification.model

data class Data(
    val content: String,
    val created_at: Any,
    val date: String,
    val id: String,
    val updated_at: Any
)