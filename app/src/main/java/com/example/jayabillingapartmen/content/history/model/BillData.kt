package com.example.jayabillingapartmen.content.history.model

data class BillData(
    val bill_number: String,
    val created_at: Any,
    val current_meter: String,
    val id: String,
    val image: String,
    val last_meter: String,
    val name: String,
    val officer_id: String,
    val unit_id: String,
    val updated_at: Any
)