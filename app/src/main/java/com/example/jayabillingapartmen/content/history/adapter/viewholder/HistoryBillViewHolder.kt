package com.example.jayabillingapartmen.content.notification.adapter.viewholder

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.example.jayabillingapartmen.*
import com.example.jayabillingapartmen.content.history.model.BillData
import com.example.jayabillingapartmen.content.notification.model.Data
import java.text.SimpleDateFormat
import java.util.*


class HistoryBillViewHolder (inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_history_report, parent, false)) {
    private var m_title: TextView? = null
    private var m_date: TextView? = null
    private var m_time: TextView? = null
    private var m_addition: TextView? = null

    private var l_utama: ConstraintLayout? = null
    private var l_addition: ConstraintLayout? = null

    init {
        m_title = itemView.findViewById(R.id.tvTitle)
        m_date = itemView.findViewById(R.id.tvDate)
        m_time = itemView.findViewById(R.id.tvTime)
        m_addition = itemView.findViewById(R.id.tvDate_addition)

        l_utama = itemView.findViewById(R.id.utama)
        l_addition = itemView.findViewById(R.id.addition)
    }

    fun bind(item: BillData) {
        if(item.id.equals("ADD")){
            l_utama?.gone()
            l_addition?.visible()

            //Set up
            val date = stringToDate(getCurrentDate(), "dd-MM-yyyy")
            val date1 = stringToDate(item.created_at.toString(), "yyyy-MM-dd")

            //Execute
            if (date == date1) {
                m_addition?.text = "Hari ini"
            }else if (getYesterdayDate() == date1){
                m_addition?.text = "Kemarin"
            }else{
                try {
                    m_addition?.text = changeDateFormat(item.created_at.toString(),
                            "yyyy-MM-dd hh:mm:ss", "dd MMM yyyy")
                }catch (e: Exception){
                    m_addition?.text = changeDateFormat(item.created_at.toString(),
                            "yyyy-MM-dd", "dd MMM yyyy")
                }
            }
        }else {
            l_utama?.visible()
            l_addition?.gone()

            //Execute
            m_title?.text = item.name
            m_date?.text = changeDateFormat(item.created_at.toString(), "yyyy-MM-dd hh:mm:ss", "dd/MM/yyyy")
            m_time?.text = changeDateFormat(item.created_at.toString(), "yyyy-MM-dd hh:mm:ss", "HH:mm")
        }
    }
}