package com.example.jayabillingapartmen.content.notification.adapter.viewholder

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.example.jayabillingapartmen.R
import com.example.jayabillingapartmen.content.notification.model.Data


class NotificationListViewHolder (inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_notification, parent, false)) {
    private var m_title: TextView? = null
    private var m_subtitle: TextView? = null

    init {
        m_title = itemView.findViewById(R.id.tvTitle)
        m_subtitle = itemView.findViewById(R.id.tvSubtitle)
    }

    fun bind(item: Data) {
        m_title?.text = item.date
        m_subtitle?.text = item.content
    }
}