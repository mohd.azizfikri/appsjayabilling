package com.example.jayabillingapartmen.content.history

import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.MenuItem
import android.widget.DatePicker
import com.example.jayabillingapartmen.*
import com.example.jayabillingapartmen.content.history.model.BillData
import com.example.jayabillingapartmen.content.history.model.HistoryBill
import com.example.jayabillingapartmen.content.notification.adapter.HistoryBillAdapter
import com.example.jayabillingapartmen.utils.ZeeDatePicker
import com.example.jayabillingapartmen.utils.`interface`.HistoryBillView
import com.example.jayabillingapartmen.utils.`interface`.onClickItem
import kotlinx.android.synthetic.main.activity_history.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class HistoryActivity : AppCompatActivity(), HistoryBillView, DatePickerDialog.OnDateSetListener {
    lateinit var presenter: HistoryActivityPresenter
    var adapterBill: HistoryBillAdapter? = null
    var menuID = 3
    var page_slide = 0
    var chose_pos = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        validationMenu()
        searchHistory()
        filterHistory()
        actionFilteringByDate()
    }

    fun validationMenu(){
        presenter = HistoryActivityPresenter(this)
        menuID = intent.getIntExtra("menu",3)

        if(menuID == 3){
            supportActionBar?.title = getString(R.string.menu3)
        }else{
            supportActionBar?.title = getString(R.string.menu4)
        }

        presenter.getTotalReport(menuID, getAccessToken(applicationContext))
    }

    fun searchHistory(){
        search_histori.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                adapterBill?.getFilter()?.filter(s)
                log("0: " +s)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                log("1: " +s)
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                log("2: " +s)
            }
        })
    }

    fun filterHistory(){
        filter_button.setOnClickListener {
            filter_layout.visible()
            main_layout.gone()
            page_slide = 1
        }

        et_start_date.setText(getCurrentDate())
        et_end_date.setText(getCurrentDate())

        et_start_date.setOnClickListener {
            chose_pos = 0
            val bundle = Bundle()
            bundle.putString("date", setDateFormat(et_start_date.text.toString(), "dd-MM-yyyy"))
            val newFragment = ZeeDatePicker()
            newFragment.setArguments(bundle)

            newFragment.show(supportFragmentManager, "Your Birthdate?")
        }

        et_end_date.setOnClickListener {
            chose_pos = 1
            val bundle = Bundle()
            bundle.putString("date", setDateFormat(et_end_date.text.toString(), "dd-MM-yyyy"))
            val newFragment = ZeeDatePicker()
            newFragment.setArguments(bundle)

            newFragment.show(supportFragmentManager, "Your Birthdate?")
        }
    }

    fun actionFilteringByDate(){
        radio_group.setOnCheckedChangeListener { group, checkedId ->
            run {
                when (checkedId) {
                    R.id.radio_newset -> {
                        // do something when radio button 1 is selected
                        zzHelp.invisible()
                        filter_date.setOnClickListener {

                        }
                    }
                    R.id.radio_customset -> {
                        // do something when radio button 2 is selected
                        zzHelp.visible()
                        filter_date.setOnClickListener {
                            presenter.getTotalReport(menuID, getAccessToken(applicationContext),
                                    et_start_date.text.toString(), et_end_date.text.toString())
                        }
                    }
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                //You can do whatever you want here
                if(page_slide == 1){
                    filter_layout.gone()
                    main_layout.visible()
                    page_slide = 0
                }else {
                    finish()
                }
                return true
            }
        }
        return false
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if(page_slide == 1){
                filter_layout.gone()
                main_layout.visible()
                page_slide = 0
            }else {
                finish()
            }
            return true
        }
        return false
    }

    override fun onGetTotalBill(item: HistoryBill) {
        //Hide Filter page
        main_layout.visible()
        filter_layout.gone()

        //Set image NOT FOUND if data is empty
        panel_not_found.gone()
        if (item.data.size == 0) {
            panel_not_found.visible()
        }

        var listHBill: ArrayList<BillData> = ArrayList()

        try {
            var date = (item.data[item.data.size-1].created_at as String).split(" ")[0]
            val smdel = BillData("", date, "",
                    "ADD", "", "","", "","","")
            listHBill.add(smdel)

            for (x in item.data.size - 1 downTo 0) {
                if(!(item.data[x].created_at as String).split(" ")[0].equals(date)){
                    date = (item.data[x].created_at as String).split(" ")[0]
                    val newmod = BillData("", SimpleDateFormat("yyyy-MM-dd").format(Date()),  "",
                            "ADD", "", "","", "","","")
                    listHBill.add(newmod)
                }
                listHBill.add(item.data[x])
            }

        }catch (e: Exception){
            listHBill = item.data
        }

        adapterBill = HistoryBillAdapter(applicationContext, listHBill, object : onClickItem {
            override fun OnClick(item: Any) {
//                val data = item as BillData
                //TODO Open detail Bill - But No design yet
            }
        })

        //Show the data
        recycler_history_bill.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = adapterBill
        }

        //Hide load after respon api is accepted
        notif_load.gone()
    }

    override fun errorResponse(cause: String) {
        toast(cause)
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        if(chose_pos == 0){
            et_start_date.text = getDateFromDatePicker(view!!)
        }else{
            et_end_date.text = getDateFromDatePicker(view!!)
        }
    }
}