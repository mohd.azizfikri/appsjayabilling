package com.example.jayabillingapartmen.content.notification

import com.example.jayabillingapartmen.CoroutineContextProvider
import com.example.jayabillingapartmen.Retrofit.ZeeRetrofit
import com.example.jayabillingapartmen.content.createreport.model.ErrorResponse
import com.example.jayabillingapartmen.content.notification.model.NotificationList
import com.example.jayabillingapartmen.log
import com.example.jayabillingapartmen.server_error
import com.example.jayabillingapartmen.utils.`interface`.NotificationListView
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationListPresenter (private val view: NotificationListView) {
    private val ctx: CoroutineContextProvider =
            CoroutineContextProvider()

    fun getNotification(accessToken: String) {
        GlobalScope.launch(ctx.default) {
            ZeeRetrofit().services.get_notification(accessToken).enqueue(object :
                    Callback<NotificationList> {
                override fun onFailure(call: Call<NotificationList>, t: Throwable) {
                    log(t.localizedMessage)
                }

                override fun onResponse(call: Call<NotificationList>, response: Response<NotificationList>) {
                    if(response.code() == 200){
                        response.body()?.let { view.onGetNotification(it) }
                    }else{
                        try {
                            val res: ErrorResponse = Gson().fromJson(response.errorBody()?.charStream(), ErrorResponse::class.java)
                            view.errorResponse(res.message)
                        }catch (e: Exception){
                            view.errorResponse(server_error)
                        }
                    }
                }
            })
        }
    }
}