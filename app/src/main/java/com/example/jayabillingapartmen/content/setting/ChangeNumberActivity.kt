package com.example.jayabillingapartmen.content.setting

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.example.jayabillingapartmen.R
import com.example.jayabillingapartmen.content.addition.ReportStatusActivity
import com.example.jayabillingapartmen.content.createreport.CreateReportPresenter
import com.example.jayabillingapartmen.database.ProfileDB
import com.example.jayabillingapartmen.getAccessToken
import com.example.jayabillingapartmen.getSelfData
import com.example.jayabillingapartmen.toast
import com.example.jayabillingapartmen.utils.`interface`.ChangeNumberView
import kotlinx.android.synthetic.main.activity_change_number.*

class ChangeNumberActivity : AppCompatActivity(), ChangeNumberView {
    private lateinit var presenter: ChangeNumberPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_number)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        presenter = ChangeNumberPresenter(this)

        val data = getSelfData(applicationContext).data
        if(!data.phone.isNullOrEmpty()){
            tvOldNumb.text = data.phone
        }else{
            tvOldNumb.text = getString(R.string.not_defined)
        }

        sumbit_change.setOnClickListener {
            presenter.change_number(getAccessToken(applicationContext), etNewNumb.text.toString())
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                //You can do whatever you want here
                finish()
                return true
            }
        }
        return false
    }

    override fun onSuccessChanging(item: Any) {
        //TODO("Not yet implemented")
        toast("Sukses")

        val bioDB = ProfileDB(applicationContext)
        bioDB.open()
        bioDB.updateAField(ProfileDB.PHONE_NUMBER, etNewNumb.text.toString())
        bioDB.close()

        val i = Intent(applicationContext, ReportStatusActivity::class.java)
        i.putExtra("trans",1)
        startActivity(i)
        finish()
    }

    override fun errorResponse(cause: String) {
        //TODO("Not yet implemented")
        toast("Gagal")
    }
}