package com.example.jayabillingapartmen.content.notification.model

data class NotificationList(
    val `data`: ArrayList<Data>,
    val message: String,
    val status: Boolean
)