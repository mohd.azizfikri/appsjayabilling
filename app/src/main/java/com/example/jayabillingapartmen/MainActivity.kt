package com.example.jayabillingapartmen

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.widget.RelativeLayout
import com.example.jayabillingapartmen.content.addition.SpanNoticationActivity
import com.example.jayabillingapartmen.content.createreport.CreateReportActivity
import com.example.jayabillingapartmen.content.history.HistoryActivity
import com.example.jayabillingapartmen.content.notification.NotificationListPresenter
import com.example.jayabillingapartmen.content.notification.adapter.BannerAdapter
import com.example.jayabillingapartmen.content.notification.adapter.NotificationListAdapter
import com.example.jayabillingapartmen.content.notification.model.Data
import com.example.jayabillingapartmen.content.notification.model.NotificationList
import com.example.jayabillingapartmen.content.setting.SettingActivity
import com.example.jayabillingapartmen.login.SignIn
import com.example.jayabillingapartmen.utils.`interface`.MainActivityView
import com.example.jayabillingapartmen.utils.`interface`.NotificationListView
import com.example.jayabillingapartmen.utils.`interface`.onClickItem
import com.example.jayabillingapartmen.utils.mainactivitymodel.TotalReport
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), MainActivityView, NotificationListView {
    private lateinit var presenter: MainActivityPresenter
    private var screenHeight: Int = 0
    private var screenWidth: Int = 0
    private var screenInches: Double = 0.0

    //Addition Notificcation
    private lateinit var presenter2: NotificationListPresenter
    var adapterNotif: NotificationListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()

        presenter = MainActivityPresenter(this)
        presenter2 = NotificationListPresenter(this)

        deviceSize()
        validationUser()
        menuIntent()
        implementDataProfile()
        banner_slide()
        filterNotification()
    }

    fun deviceSize(){
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        screenHeight = displayMetrics.heightPixels
        screenWidth = displayMetrics.widthPixels
        val y = Math.pow((screenHeight/displayMetrics.xdpi).toDouble(), 2.0)
        val x = Math.pow((screenWidth/displayMetrics.xdpi).toDouble(), 2.0)
        screenInches = Math.sqrt(x+y)
        screenInches = Math.round(screenInches * 10 / 10).toDouble()

        //Layout param for Base Menu
        val rely = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, screenHeight - 60)
        base.layoutParams = rely

        //Layout param for Notification (below of the base menu)
        val rely2 = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, screenHeight - 60)
        rely2.addRule(RelativeLayout.BELOW, R.id.sparator_1);
        notif.layoutParams = rely2
    }

    override fun onResume() {
        super.onResume()
        validationUser()
    }

    fun validationUser(){
        if(getAccessToken(applicationContext).equals(no_bearer)){
            val intent = Intent(this, SignIn::class.java)
            startActivity(intent)
            finish()
        }else{
            //Get data after loginValidation is OK
            presenter.getTotalReport(getAccessToken(applicationContext))
            presenter2.getNotification(getAccessToken(applicationContext))
        }
    }

    fun menuIntent(){
        menu1.setOnClickListener {
            val intent = Intent(this, CreateReportActivity::class.java)
            intent.putExtra("menu",1)
            startActivity(intent)
        }
        menu2.setOnClickListener {
            val intent = Intent(this, CreateReportActivity::class.java)
            intent.putExtra("menu",2)
            startActivity(intent)
        }
        menu3.setOnClickListener {
            val intent = Intent(this, HistoryActivity::class.java)
            intent.putExtra("menu",3)
            startActivity(intent)
        }
        menu4.setOnClickListener {
            val intent = Intent(this, HistoryActivity::class.java)
            intent.putExtra("menu",4)
            startActivity(intent)
        }
        menu5.setOnClickListener {
//            val intent = Intent(this, NotificationListActiivity::class.java)
//            startActivity(intent)
            scrollToView(scr_view, notif)
        }
        menu6.setOnClickListener {
            val intent = Intent(this, SettingActivity::class.java)
            startActivity(intent)
        }
    }

    fun implementChart(electric: Int, water: Int){
        //Chart Report Listrik
        chartListrik.setMinValue(0f)
        chartListrik.setMaxValue(100f)
        var num_electric = electric
        if(electric >= 100) {
            num_electric = 100
            numChartListrik.text = getString(R.string.max_chart_number)
        }else {
            numChartListrik.text = num_electric.toString()
        }
        chartListrik.setValue(num_electric.toFloat())

        //Chart Report Air
        chartAir.setMinValue(0f)
        chartAir.setMaxValue(100f)
        var num_water = water
        if (water >= 100){
            num_water = 100
            numChartAir.text = getString(R.string.max_chart_number)
        }else{
            numChartAir.text = num_water.toString()
        }
        chartAir.setValue(num_water.toFloat())
    }

    @SuppressLint("SetTextI18n")
    fun implementDataProfile(){
        val bioDT = getSelfData(applicationContext)
        nameProfile.text = "Halo, "+ bioDT.data.username + "!"
        nameProfile2.text = bioDT.data.officer_name
        nameUnit.text = bioDT.data.unit.name
        if(bioDT.data.avatar.toString().startsWith("http")) {
            Picasso.with(applicationContext)
                .load(bioDT.data.avatar.toString())
                .transform(ZeeCircleTransform(100, 0))
                .fit()
                .into(imgProfile)
        }
    }

    fun banner_slide(){
        var currentPage = 0
        val timer: Timer
        val DELAY_MS: Long = 1000    // delay in milliseconds before task is to be executed
        val PERIOD_MS: Long = 7000  // time in milliseconds between successive task executions.

        vp_banner.adapter = BannerAdapter(this)

        /*After setting the adapter use the timer */
        val handler = Handler()
        val Update = Runnable() {
            run {
                if (currentPage == 1) {
                    currentPage = 0
                }else{
                    currentPage++
                }
                vp_banner?.setCurrentItem(currentPage, true)
            }
        }

        timer = Timer() // This will create a new Thread
        timer.schedule(object : TimerTask() { // task to be scheduled
            override fun run() {
                handler.post(Update)
            }
        }, DELAY_MS, PERIOD_MS)
    }

    fun filterNotification(){
        historySearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                adapterNotif?.getFilter()?.filter(s)
                log("0: " +s)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                log("1: " +s)
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                log("2: " +s)
            }
        })
        /*setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                adapterNotif?.getFilter()?.filter(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapterNotif?.getFilter()?.filter(newText)
                return true
            }

        })*/
    }

    override fun onGetTotalReport(item: TotalReport) {
        implementChart(Integer.parseInt(item.data.electricity.total),
                Integer.parseInt(item.data.water.total))
    }

    override fun onGetNotification(item: NotificationList) {
        adapterNotif = NotificationListAdapter(applicationContext, item.data, object : onClickItem {
            override fun OnClick(item: Any) {
                val data = item as Data

                val intent = Intent(applicationContext, SpanNoticationActivity::class.java)
                intent.putExtra("title", data.date)
                intent.putExtra("content", data.content)
                startActivity(intent)
            }
        })

        recycle_history.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = adapterNotif
        }

        notif_load.gone()
    }

    override fun errorResponse(cause: String) {
        toast(cause)
    }
}