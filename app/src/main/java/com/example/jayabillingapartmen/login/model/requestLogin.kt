package com.example.jayabillingapartmen.login.model

data class requestLogin(
    val username: String,
    val password: String
)
