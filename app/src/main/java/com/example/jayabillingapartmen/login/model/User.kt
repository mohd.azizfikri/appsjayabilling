package com.example.jayabillingapartmen.login.model

data class User(
    val avatar: Any,
    val created_at: Any,
    val id: String,
    val officer_name: String,
    val phone: String,
    val updated_at: Any,
    val username: String
)