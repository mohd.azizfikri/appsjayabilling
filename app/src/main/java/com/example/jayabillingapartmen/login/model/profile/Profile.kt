package com.example.jayabillingapartmen.login.model.profile

data class Profile(
    val `data`: Data,
    val message: String,
    val status: Boolean
)