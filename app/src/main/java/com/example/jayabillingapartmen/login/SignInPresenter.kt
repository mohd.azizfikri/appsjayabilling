package com.example.jayabillingapartmen.login

import com.example.jayabillingapartmen.CoroutineContextProvider
import com.example.jayabillingapartmen.Retrofit.ZeeRetrofit
import com.example.jayabillingapartmen.content.createreport.model.ErrorResponse
import com.example.jayabillingapartmen.log
import com.example.jayabillingapartmen.login.model.profile.Profile
import com.example.jayabillingapartmen.login.model.requestLogin
import com.example.jayabillingapartmen.login.model.responLogin
import com.example.jayabillingapartmen.server_error
import com.example.jayabillingapartmen.utils.`interface`.SignInView
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignInPresenter (private val view: SignInView) {
    private val ctx: CoroutineContextProvider =
            CoroutineContextProvider()

    fun login(username: String, password: String){
        GlobalScope.launch(ctx.default) {
            ZeeRetrofit().services.login(requestLogin(username, password)).enqueue(object :
                    Callback<responLogin> {
                override fun onFailure(call: Call<responLogin>, t: Throwable) {
                    log(t.localizedMessage)
                }

                override fun onResponse(call: Call<responLogin>, response: Response<responLogin>) {
                    if(response.code() == 200){
                        response.body()?.let { view.onLoginSuccess(it) }
                    }else{
                        try {
                            val res: ErrorResponse = Gson().fromJson(response.errorBody()?.charStream(), ErrorResponse::class.java)
                            view.errorResponse(res.message)
                        }catch (e: Exception){
                            view.errorResponse(server_error)
                        }
                    }
                }
            })
        }
    }

    fun getProfile(authorize: String){
        GlobalScope.launch(ctx.default) {
            ZeeRetrofit().services.get_profile("Bearer " + authorize).enqueue(object :
                    Callback<Profile> {
                override fun onFailure(call: Call<Profile>, t: Throwable) {
                    log(t.localizedMessage)
                }

                override fun onResponse(call: Call<Profile>, response: Response<Profile>) {
                    if(response.code() == 200){
                        response.body()?.let { view.onGetProfile(authorize, it) }
                    }else{
                        try {
                            val res: ErrorResponse = Gson().fromJson(response.errorBody()?.charStream(), ErrorResponse::class.java)
                            view.errorResponse(res.message)
                        }catch (e: Exception){
                            view.errorResponse(server_error)
                        }
                    }
                }

            })
        }
    }
}