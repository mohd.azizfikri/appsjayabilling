package com.example.jayabillingapartmen.login

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.jayabillingapartmen.*
import com.example.jayabillingapartmen.database.ProfileDB
import com.example.jayabillingapartmen.login.model.profile.Profile
import com.example.jayabillingapartmen.login.model.responLogin
import com.example.jayabillingapartmen.utils.`interface`.SignInView
import kotlinx.android.synthetic.main.activity_sign_in.*

class SignIn : AppCompatActivity(), SignInView {
    private lateinit var presenter: SignInPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        supportActionBar?.hide()

        presenter = SignInPresenter(this)
        deleteAllData(applicationContext)

        submit.setOnClickListener{
            if(ed_usname.text.isNullOrBlank()){
                toast("Harap mengisi username terlebih dahulu!")
            }else if(ed_password.text.isNullOrBlank()){
                toast("Harap mengisi password terlebih dahulu!")
            }else{
                presenter.login(
                    ed_usname.text.toString(),
                    ed_password.text.toString()
                )
            }
        }
    }

    override fun onLoginSuccess(item: responLogin) {
        //Access database
        val dbHelper = ProfileDB(applicationContext)
        val db = dbHelper.getWritableDatabase()
        val dataLOG = arrayOf(item.data.token)
        db?.execSQL(getString(R.string.sql_insert_token), dataLOG)

        presenter.getProfile(item.data.token)
    }

    override fun onGetProfile(token: String, item: Profile) {
        val bioDB = ProfileDB(applicationContext)
        bioDB.open()

        bioDB.updateAField(ProfileDB.ACCESS_TOKEN, token)
        bioDB.updateAField(ProfileDB.FULLNAME, item.data.officer_name  ?: "")
        bioDB.updateAField(ProfileDB.USERNAME, item.data.username  ?: "")
        bioDB.updateAField(ProfileDB.USER_ID, item.data.id  ?: "")
        bioDB.updateAField(ProfileDB.AVATAR, (item.data.avatar  ?: "").toString())
        bioDB.updateAField(ProfileDB.PHONE_NUMBER, item.data.phone  ?: "")
        bioDB.updateAField(ProfileDB.UNIT_ID, item.data.unit.id  ?: "")
        bioDB.updateAField(ProfileDB.UNIT_NAME, item.data.unit.name  ?: "")
        bioDB.updateAField(ProfileDB.OFFICER_ID, item.data.unit.officer_id  ?: "")
        bioDB.updateAField(ProfileDB.TOWER_ID, item.data.unit.tower_id  ?: "")
        bioDB.updateAField(ProfileDB.UNIT_EMAIL, item.data.unit.email  ?: "")
        bioDB.updateAField(ProfileDB.UNIT_PHONE, item.data.unit.phone  ?: "")

        bioDB.close()

        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("id",1)
        startActivity(intent)
        finish()
    }

    override fun errorResponse(cause: String) {
        toast(cause)
    }
}