package com.example.jayabillingapartmen.login.model

data class Data(
    val token: String,
    val user: User
)