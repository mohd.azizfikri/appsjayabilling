package com.example.jayabillingapartmen.login.model.profile

data class Unit(
    val created_at: Any,
    val email: String,
    val id: String,
    val name: String,
    val officer_id: String,
    val owner_name: String,
    val phone: String,
    val tower_id: String,
    val updated_at: Any
)