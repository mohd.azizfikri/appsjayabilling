package com.example.jayabillingapartmen.login.model

data class responLogin(
    val `data`: Data,
    val message: String,
    val status: Boolean
)