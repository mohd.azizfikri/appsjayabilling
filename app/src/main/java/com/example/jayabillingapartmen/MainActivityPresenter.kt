package com.example.jayabillingapartmen

import com.example.jayabillingapartmen.Retrofit.ZeeRetrofit
import com.example.jayabillingapartmen.content.createreport.model.ErrorResponse
import com.example.jayabillingapartmen.utils.`interface`.MainActivityView
import com.example.jayabillingapartmen.utils.mainactivitymodel.TotalReport
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivityPresenter(private val view: MainActivityView) {
    private val ctx: CoroutineContextProvider =
            CoroutineContextProvider()

    fun getTotalReport(token: String){
        GlobalScope.launch(ctx.default) {
            ZeeRetrofit().services.get_total_report( token).enqueue(object :
                    Callback<TotalReport> {
                override fun onFailure(call: Call<TotalReport>, t: Throwable) {
                    log(t.localizedMessage)
                }

                override fun onResponse(call: Call<TotalReport>, response: Response<TotalReport>) {
                    if(response.code() == 200){
                        response.body()?.let {
                            view.onGetTotalReport(it)
                        }
                    }else{
                        try {
                            val res: ErrorResponse = Gson().fromJson(response.errorBody()?.charStream(), ErrorResponse::class.java)
                            view.errorResponse(res.message)
                        }catch (e: Exception){
                            view.errorResponse(server_error)
                        }
                    }
                }

            })
        }
    }
}