package com.example.jayabillingapartmen.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.jayabillingapartmen.login.model.profile.Data
import com.example.jayabillingapartmen.login.model.profile.Profile
import com.example.jayabillingapartmen.login.model.profile.Unit
import java.sql.SQLException


class ProfileDB(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    private var database: SQLiteDatabase? = null

    private val CREATE_EVENT_TABLE = "CREATE TABLE " + TABLE_PROFILE + " (" +
            ID + " INTEGER PRIMARY KEY, " +
            ACCESS_TOKEN + " TEXT, " +
            FIREBASE_TOKEN + " TEXT, " +
            FULLNAME + " TEXT, " +
            USERNAME + " TEXT, " +
            USER_ID + " TEXT, " +
            AVATAR + " TEXT, " +
            PHONE_NUMBER + " TEXT, " +
            UNIT_ID + " TEXT, " +
            OFFICER_ID + " TEXT, " +
            TOWER_ID + " TEXT, " +
            UNIT_NAME + " TEXT, " +
            UNIT_OWNER + " TEXT, " +
            UNIT_EMAIL + " TEXT, " +
            UNIT_PHONE + " TEXT " +
            ")"

    private val DROP_USER_TABLE = "DROP TABLE IF EXISTS $TABLE_PROFILE"

    @Throws(SQLException::class)
    fun open(): ProfileDB {
        database = writableDatabase
        return this
    }

    override fun close() {
        database?.close()
    }

    val select_id: Cursor?
        get() {
            val cursor = getDatabase().query(TABLE_PROFILE, arrayOf(ID), null, null, null, null, null)

            cursor?.moveToFirst()

            return cursor
        }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(CREATE_EVENT_TABLE)
        database = db
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL(DROP_USER_TABLE)
        onCreate(db)
    }


    private fun getDatabase(): SQLiteDatabase {
        if (database == null) {
            database = writableDatabase
        }
        return database!!
    }


    fun insertDataProfile(token: String, model: Profile) {
        val cv = ContentValues()
        cv.put(ACCESS_TOKEN, token)
        cv.put(FULLNAME, model.data.officer_name)
        cv.put(USERNAME, model.data.username)
        cv.put(USER_ID, model.data.id)
        cv.put(AVATAR, model.data.avatar.toString())
        cv.put(PHONE_NUMBER, model.data.phone)
        cv.put(UNIT_ID, model.data.unit.id)
        cv.put(USER_ID, model.data.unit.officer_id)
        cv.put(TOWER_ID, model.data.unit.tower_id)
        cv.put(UNIT_NAME, model.data.unit.name)
        cv.put(UNIT_OWNER, model.data.unit.owner_name)
        cv.put(UNIT_EMAIL, model.data.unit.email)
        cv.put(UNIT_PHONE, model.data.unit.phone)
        database?.insert(TABLE_PROFILE, null, cv)
    }

    @Throws(SQLException::class)
    fun retrieveRooms(): ArrayList<Profile> {
        val listMemberCards = ArrayList<Profile>()
        val cur = database?.query(
                true,
                TABLE_PROFILE,
                arrayOf<String>(
                        ID,
                        ACCESS_TOKEN,
                        FIREBASE_TOKEN,
                        FULLNAME,
                        USERNAME,
                        USER_ID,
                        AVATAR,
                        PHONE_NUMBER,
                        UNIT_ID,
                        OFFICER_ID,
                        TOWER_ID,
                        UNIT_NAME,
                        UNIT_OWNER,
                        UNIT_EMAIL,
                        UNIT_PHONE
                ),
                null,           // ROOMS_TYPE + "= '" + type + "'"
                null,
                null,
                null,
                null,
                null
        )
        if (cur!!.moveToFirst()) {
            do {
                val unit = Unit(
                        "",
                        cur.getString(cur.getColumnIndex(UNIT_EMAIL)),
                        cur.getString(cur.getColumnIndex(UNIT_ID)),
                        cur.getString(cur.getColumnIndex(UNIT_NAME)),
                        cur.getString(cur.getColumnIndex(OFFICER_ID)),
                        cur.getString(cur.getColumnIndex(UNIT_OWNER)),
                        cur.getString(cur.getColumnIndex(UNIT_PHONE)),
                        cur.getString(cur.getColumnIndex(TOWER_ID)),
                        ""
                )
                val data = Data(
                        cur.getString(cur.getColumnIndex(AVATAR)),
                        "",
                        cur.getString(cur.getColumnIndex(USER_ID)),
                        cur.getString(cur.getColumnIndex(FULLNAME)),
                        cur.getString(cur.getColumnIndex(PHONE_NUMBER)),
                        unit , //model unit get(from the same db)
                        "",
                        cur.getString(cur.getColumnIndex(USERNAME))
                )
                val model = Profile(
                        data, //model data get(from the same db)
                        "Get from local database success!",
                        true
                )

                listMemberCards.add(model)
            } while (cur.moveToNext())
        }
        return listMemberCards
    }

    fun updateAField(dField: String, dValue: String) {
        try {
            val cv = ContentValues()
            cv.put(dField, dValue)
            getDatabase().update(TABLE_PROFILE, cv, ID + "=?", arrayOf("1"))
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
        }
    }

    fun deleteAllData() {
        getDatabase().delete(ProfileDB.TABLE_PROFILE, null, null) > 0
    }

    fun dropDB(){
        val query_dropTable = "DROP TABLE IF EXISTS $TABLE_PROFILE"

        getDatabase().execSQL(query_dropTable)
        onCreate(getDatabase())
    }
    @Throws(SQLException::class)
    fun retrieveAccessToken(id: String): String {
        var aksToken = String()
        val cur = database?.query(true, TABLE_PROFILE, arrayOf<String>(ID, ACCESS_TOKEN),
                ID + "= '" +id + "'",null,null,null,
                null, null)
        if (cur!!.moveToFirst()) {
            do {
                val token = cur.getString(cur.getColumnIndex(ACCESS_TOKEN)) ?: ""

                aksToken = token
            } while (cur.moveToNext())
        }
        return aksToken
    }

    @Throws(SQLException::class)
    fun retrieveRoomsbyID(id: String): Profile {
        var listMemberCards =
                Profile(Data("","","","","", Unit("",
                        "","","","","","","",""),
                        "",""),"No data entry", false
                )
        val cur = database?.query(
                true,
                TABLE_PROFILE,
                arrayOf<String>(
                        ID,
                        ACCESS_TOKEN,
                        FIREBASE_TOKEN,
                        FULLNAME,
                        USERNAME,
                        USER_ID,
                        AVATAR,
                        PHONE_NUMBER,
                        UNIT_ID,
                        OFFICER_ID,
                        TOWER_ID,
                        UNIT_NAME,
                        UNIT_OWNER,
                        UNIT_EMAIL,
                        UNIT_PHONE
                ),
                ID + "= '" +id + "'",
                null,
                null,
                null,
                null,
                null
        )
        if (cur!!.moveToFirst()) {
            do {
                val unit = Unit(
                        "",
                        cur.getString(cur.getColumnIndex(UNIT_EMAIL)) ?: "",
                        cur.getString(cur.getColumnIndex(UNIT_ID)) ?: "",
                        cur.getString(cur.getColumnIndex(UNIT_NAME)) ?: "",
                        cur.getString(cur.getColumnIndex(OFFICER_ID)) ?: "",
                        cur.getString(cur.getColumnIndex(UNIT_OWNER)) ?: "",
                        cur.getString(cur.getColumnIndex(UNIT_PHONE)) ?: "",
                        cur.getString(cur.getColumnIndex(TOWER_ID)) ?: "",
                        ""
                )
                val data = Data(
                        cur.getString(cur.getColumnIndex(AVATAR)) ?: "",
                        "",
                        cur.getString(cur.getColumnIndex(USER_ID)) ?: "",
                        cur.getString(cur.getColumnIndex(FULLNAME)) ?: "",
                        cur.getString(cur.getColumnIndex(PHONE_NUMBER)) ?: "",
                        unit , //model unit get(from the same db)
                        "",
                        cur.getString(cur.getColumnIndex(USERNAME)) ?: ""
                )
                val model = Profile(
                        data, //model data get(from the same db)
                        "Get from local database success!",
                        true
                )

                listMemberCards = model
            } while (cur.moveToNext())
        }
        return listMemberCards
    }

    fun query(rawQuery: String, args: Array<String>): Cursor {
        return getDatabase().rawQuery(rawQuery, args)
    }

    fun execSql(sql: String, args: Array<String>?) {
        if (args == null) {
            getDatabase().execSQL(sql)
        } else {
            getDatabase().execSQL(sql, args)
        }
    }

    fun getColValue(keyword: String): String {
        val cursor = getDatabase().query(TABLE_PROFILE, arrayOf(keyword), null, null, null, null, null)

        cursor?.moveToFirst()
        return cursor!!.getString(cursor.getColumnIndexOrThrow(keyword))
    }

    companion object {

        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "bioData.db"
        val TABLE_PROFILE = "profile_db"
        private var instance: ProfileDB? = null
        val ID = "ID"
        val ACCESS_TOKEN = "ACCESS_TOKEN"
        val FIREBASE_TOKEN = "FIREBASE_TOKEN"
        val FULLNAME = "FULLNAME"
        val USERNAME = "USERNAME"
        val USER_ID = "USER_ID"
        val AVATAR = "AVATAR"
        val PHONE_NUMBER = "PHONE_NUMBER"
        val UNIT_ID = "UNIT_ID"
        val OFFICER_ID = "OFFICER_ID"
        val TOWER_ID = "TOWER_ID"
        val UNIT_NAME = "UNIT_NAME"
        val UNIT_OWNER = "UNIT_OWNER"
        val UNIT_EMAIL = "UNIT_EMAIL"
        val UNIT_PHONE = "UNIT_PHONE"

        @Synchronized
        fun getInstance(
                context: Context
        ): ProfileDB {
            if (instance == null) {
                instance = ProfileDB(
                        context
                )
            }
            return instance as ProfileDB
        }
    }

}
