package com.example.jayabillingapartmen.splash

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.example.jayabillingapartmen.MainActivity
import com.example.jayabillingapartmen.R
import java.util.ArrayList

class SplashScreen : AppCompatActivity() {
    //Permission Array
    private val ALL_PERMISSIONS_RESULT = 107
    private var permissionsToRequest: ArrayList<String>? = null
    private var permissionsRejected: ArrayList<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        supportActionBar?.hide()

        //Try to make a full screen windows page
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }catch (e: Exception){
            e.printStackTrace()
        }

        // Here, Get Permission on this App
        cekPermision()

        //Preparing android apps
        val handler = Handler()
        handler.postDelayed(Runnable {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }, 5000) // 1000ms = 1 detik
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {

        when (requestCode) {
            ALL_PERMISSIONS_RESULT -> {
                var someAccepted = false
                var someRejected = false
                for (perms in permissionsToRequest!!) {
                    if (hasPermission(perms)) {
                        someAccepted = true
                    } else {
                        someRejected = true
                        permissionsRejected!!.add(perms)
                    }
                }

                if (permissionsRejected!!.size > 0) {
                    someRejected = true
                }

                if (someAccepted) {
                    //  permissionSuccess.setVisibility(View.VISIBLE);
                    someRejected = false
                }
                if (someRejected) {
                    //   makePostRequestSnack();
                    permissionsRejected!!.clear()
                    someRejected = false

                    Toast.makeText(
                            this,
                            getString(R.string.permission_request_title),
                            Toast.LENGTH_SHORT
                    ).show()

                    val handler = Handler()
                    handler.postDelayed({ cekPermision() }, (2 * 1000).toLong())
                }
            }
        }
    }

    private fun cekPermision() {
        val permissions = ArrayList<String>()
        var resultCode = 0
        permissions.add(Manifest.permission.CAMERA)
        permissions.add(Manifest.permission.INTERNET)
        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)

        resultCode = ALL_PERMISSIONS_RESULT
        permissionsToRequest = findUnAskedPermissions(permissions)
        permissionsRejected = findRejectedPermissions(permissions)

        if (permissionsToRequest!!.size > 0) {//we need to ask for permissions
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsToRequest!!.toTypedArray(), resultCode)
            }
        }
    }

    private fun hasPermission(permission: String): Boolean {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return checkSelfPermission(permission) === PackageManager.PERMISSION_GRANTED
            }

        }
        return true
    }

    private fun findUnAskedPermissions(wanted: ArrayList<String>): ArrayList<String> {
        val result = ArrayList<String>()
        for (perm in wanted) {
            result.add(perm)
        }

        return result
    }

    private fun findRejectedPermissions(wanted: ArrayList<String>): ArrayList<String> {
        val result = ArrayList<String>()
        for (perm in wanted) {
            result.add(perm)
        }
        return result
    }

    private fun canMakeSmores(): Boolean {
        return Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1
    }
}